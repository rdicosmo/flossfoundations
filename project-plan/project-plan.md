# FLOSS Foundations website revamp

## Goal

Transition the FLOSS Foundations site to a simple but modern static site generator, updating some of the content in the process.

## Background

The [foundation directory](http://flossfoundations.org/foundation-directory) is incredibly out of date. It's a shame that this valuable resource is neglected.

I wanted to start a project to update the directory, but that would require creating an account on the wow-so-old site. I'm not against that on principle, but it does seem like a _minor hassle_. Bah. Who needs a minor hassle?

On the other hand, converting the entire site to a static site generator would be a _major hassle_, but it would come with benefits for the community. The site would be up to date (finally), and using well-known tools like GitLab (or GitHub) would make it easier for people to contribute updates…or at least report issues and feature requests more easily.

So a full site conversion it is. Onward and upward.

## Project pieces

In no particular order (yet).

* Review existing site for pages, etc
    1. Front page
        * link to ML
        * link to blogs (planet.flossfoundations.org) -> 404s
    1. Foundation Directory
    1. About page (freelibreopen-source-software-foundations)
        * Content currently duplicated on front page
    1. Previous meetings (currently only listed in the side bar)
        * OSCON 2014 (oscon2014)
        * FOSDEM 2013 (fosdem-2013)
        * FOSDEM 2012 (fosdem-2012)
        * OSCON 2009 (oscon-2009)
        * SCaLE 2009 (floss-foundations-scale-2009)
        * OSCON 2008 (floss-foundations-oscon-2008)
        * FOSDEM 2007 (node/5)
        * OSCON 2006 (node/6)
        * FOSDEM 2006 (node/10)
        * EuroOSCON 2005 (node/11)
        * OSCON 2005 (node/12)
    1. Blog posts
        * Best of the Foundations mailing list (best-foundations-mailing-list)
        * FLOSS Foundations Software Project (floss-foundations-software-project)
        * Meet the Funders Planning Page (meet-funders-planning-page)
        * Should I Start A Non-Profit Organization (NPO) for my FLOSS Project? (starting-a-nonprofit)
    1. Blog categories
        * Meetings (category/page-type/meetings)
            * Several posts here that aren't on the meeting list in the side bar
        * Announcements (category/page-type/announcements)
            * Only two posts and both are listed in the blog posts
    1. Site search capability
* Plan for pages (old and new)
    1. Header menu bar
        * About
            * Revamp current About text
            * What even is
            * Mission
            * History
            * Founders
        * Blog
        * Categories
            * Just the blog categories
        * Mailing List Archives
            * Link directly to archives
            * Will only work for members
        * Contribute!
            * New page
            * How to contribute to the site
                * Open issues
                * Submit blog posts
                * Send MRs for fixes
                * Code of conduct, yo
        * Code of Conduct
            * Link to CoC

    1. Front page
        * Hero image-ish thing
        * Brief "what even is" statement
        * Three main sections
            * Foundation Directory
            * Mailing list: join & view archives
            * IRC channel
                * New page: IRC info, quickstart, link to freenode web, link to CoC
        * Blog posts?
    1. Foundation Directory
        * For now just convert to Markdown
        * Updating content will be phase 2
            * Add sections: Current, Defunct
    1. Footer
        * Copyright, licensing
        * Contribute
        * CoC
        * Search
        * RSS
* Move blog posts to an archive, but otherwise dump the blog (it's moribund)?
    * No, have new blog
    * Move previous posts there
    * Folks can send MR to add new posts
