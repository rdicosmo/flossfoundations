---
layout: single
title: "FLOSS Foundations at OSCON 2006"
date: 2006-07-01
permalink: /node/6
categories:
    - meetings
---

The summit is a place for the people who try to Get Stuff Done at the various non-profits around open source to congregate, share information, and pick each others' brains.

## When? Where?

July 24 and 25 in Portland, Oregon (USA). The summit will be held immediately before [OSCON](http://conferences.oreillynet.com/os2006), at the [Oregon Convention Center](http://www.oregoncc.org/), room D133. We'll meet from 9am-5pm, with a long lunch break downtown.

## Who?

Confirmed attendees:

* Allison Randal, board of directors for the Perl Foundation (organizer of the summit)
* Zak Greant, Mozilla Foundation staff (See http://zak.greant.com/affiliations for a full list of affiliations)
* Louis Suarez-Potts, Community Manager at OpenOffice.org
* Mike Milinkovich, Eclipse Foundation
* Ted Leung, Open Source Applications Foundation and Apache Software Foundation
* David Ascher, Python Software Foundation
* Cliff Schmidt, Apache Software Foundation (Monday only)
* Corey Shields, OSU Open Source Lab
* Jo Walsh, Open Source Geospatial Foundation
* Richard Dice, The Perl Foundation
* Alex Russell, Dojo Foundation
* Kaliya Hamlin, Planetworks

Invited:

* FLOSS foundations mailing list.
* Sheila Warren, [Silk, Adler & Colvin](http://www.silklaw.com/) (non-profit law)

Unable to attend:

* Frank Hecker, Mozilla Foundation, Executive Director

## Agenda

These are the proposed agenda items. Add your own suggestions.

### Day 1: Presentations

* Kaliya Hamlin on processes for FLOSS communities: This interactive session offers insight and experience in the art of how to lead participatory processes to make the best of them. We will cover a range of topics, including: the art of invitation; creating space; 'icebreakers'; productive collaborative processes (examples include open space technology and appreciative inquiry); and closings. Attention will also be given to difficult communication patterns that can arise and ways to deal with them. Notes at [Processes For Floss Communities](/node/7)
* Lightning talks on organizational infrastructure and processes. Each shows a piece of technology or process that saves us time or makes us more effective. Many of us have similar needs for processes and information management tools.
* Zak Greant on how using bug trackers for managing community issues saves time, creates better solutions and helps prevent trouble.
* Justin Erenkrantz on managing the finances of an Open Source non-profit. The creation of a foundation around an open-source project can provide long-term viability and stability to a project. Essential to this stability is proper management of finances and adherence to law. Most software developers aren't familiar with the complex web of law and regulations that governs a non-profit and its finances. Topics that will be discussed:
    * Why the Formation of a Corporation is Important
    * Articles of Incorporation and Bylaws
    * Board of Directors, Officers
    * For-profit and Non-profit?
    * Choosing the Right Jurisdictions
    * Is it worth it to seek 501(c)3 status?
    * Public Charities and Private Foundations
    * Limitations on non-profits
    * Defining a sponsorship program and attracting sponsors
    * Soliciting electronic contributions
    * Acknowledging contributions
    * The right services to look for from a bank
    * Filing Annual Returns
    * Seeking Legal Advice
    * D&O Insurance

Notes at [Managing The Finances of an Open Source Non-Profit](/node/8)

* Cliff Schmidt on recent ASF policies such as crypto export controls (e.g. OpenSSL), copyright notices for collaborative projects that don't require copyright assignment, and acceptable licenses for third-party components distributed with a project. Will also give examples of subtle wording differences in open source patent licenses and how they impact collaboration at open source foundations. The information behind Cliff's discussion can be found at http://apache.org/legal.
* Corey Shields on the OSU Open Source Lab: What we have done and will be doing in the future, how it works, the economics behind it, and why we are so much more than just another colo. [Read the notes from this discussion](/node/9).

### Day 2: Open Discussion

* Round table introductions
* Buyer beware: what should the average community member be able to expect from their foundation?
* GPLv3?

### ! Misc. Notes:

* [Maturity Models for Open Source](https://blogs.eclipse.org/post/mike-milinkovich/maturity-models-open-source-adoption)
* [Open Business Readiness Rating](http://web.archive.org/web/20060702021910/http://www.openbrr.org/wiki/index.php/Home)
* Great Book: [How to Change the World](https://global.oup.com/academic/product/how-to-change-the-world-9780195334760?cc=us&lang=en&)
* Jo Walsh's followup post on the meeting: http://blog.okfn.org/2006/08/01/geodata_in_open_source_foundation/
