---
layout: single
title: "FLOSS Foundations at OSCON 2008"
date: 2008-07-01
permalink: /floss-foundations-oscon-2008
categories:
    - meetings
---

The face-to-face meetings are a place for the people who try to Get Stuff Done at various open source projects and non-profits around open source to congregate, share information, and pick each others' brains. This year we met July 19th and 20th in Portland, Oregon (USA). The meeting will be held immediately before [OSCON](http://conferences.oreillynet.com/oscon), at the [Oregon Convention Center](http://www.oregoncc.org/) in room D132.

Read the [notes](/notes-floss-foundations-oscon-2008).

Saturday - [Attendee Layout](/node/14)

## Confirmed attendees

* Allison Randal, board of directors for the Perl Foundation and the Parrot Foundation (organizer of the summit)
* Danese Cooper, OSI board member, Apache Software Foundation member, Mozilla Foundation advisory board member
* Gervase Markham, Mozilla Foundation staff member (Saturday only)
* Henri Yandell, former Apache Software Foundation board member.
* Matt Lee, Campaigns Manager, Free Software Foundation.
* Zak Greant, Mozilla Foundation staff member; OSI board observer; FSF volunteer
* Dave Neary, GNOME Foundation member, Foundations list creator
* Donald Smith, Director of Ecosystem Development, The Eclipse Foundation.
* Luis Villa, GNOME Foundation Director, irritating proto-lawyer
* Simon Phipps, Sun open source guy and GNOME Foundation advisory board among others
* Scott Kveton, OpenID Foundation board member, bacon lover
* Karl Fogel, Subversion Corporation board member, QuestionCopyright.org board member
* Bradley M. Kuhn, President, [Software Freedom Conservancy](http://sfconservancy.org). FLOSS Community Liaison, [Software Freedom Law Center](http://www.softwarefreedom.org/).
* Karen Sandler, Counsel, Software Freedom Law Center. Officer, Software Freedom Conservancy.
* Jacob Kaplan-Moss, President, Django Software Foundation
* Josh Berkus, Core Team, PostgreSQL & Assistant Treasurer, SPI (Saturday only)
* Jerry Gay, President, Parrot Foundation (Sunday only)
* Dalibor Topic, Sun's Java F/OSS dude, and sticker-on-the-laptop carrying FSF member (Sunday only)
* Kaliya Hamlin, Identity Commons Steward, Planetwork Network Director - FOSS Leadership Training (Saturday Only)
* Michael Dexter, BSD Fund Program Manager
* Lance Albertson, OSU Open Source Lab, Lead Systems Admin (Saturday)
* Greg Lund-Chaix, OSU Open Source Lab, Project Manager (Sunday)
* David Mandel, Executive Director, Linux Fund
* Aaron Williamson, Counsel, Software Freedom Law Center. (Sunday only)
* Alex Russell, President, Dojo Foundation

## Invited

* FLOSS foundations mailing list.
* _Add others you invite._

## Agenda

These are the proposed agenda items. Add your own suggestions. (Times listed are approximate.)

### Day 1: Round Table Discussion

* 9:00am - 9:30am Introductions
* FLOSS for NPO donation systems: Following up from last year on the discussion of software for managing NPOs, bkuhn can update on trials of [LedgerSMB](https://ledgersmb.org) for org financial management.
* State of FLOSS for NPO management: discussions about launching an NPO management project, and hopefully there can be reports from Zak on OSI's work with Trac and David Boswell on Mozilla's use of [CiviCRM](http://civicrm.org/) (I haven't asked them yet; I've just drafted them. -- bkuhn)
* [Identity Commons](http://wiki.idcommons.net/Main_Page) organizational form - i think the model we have innovated to support the explicitly collaborative related efforts including a range of organizational forms - independent nonprofits (OpenID Foundation, Information Card Foundation, XDI.org) , software and business efforts that are 'under' other legal entities (like Higgins of Eclipse and VRM of Berkman) and groups of people self organizing - like Open Source Identity Systems and others - all working on a common purpose with shared principles. (Kaliya Hamlin, [Identity Woman](https://identitywoman.net), Identity Commons Steward)
* [FOSS Leadership Training](http://web.archive.org/web/20090129120732/http://www.planetwork.net/fossleaders) - in Feb 2009 - conversation about the training, getting advisers on broad interest in participating. (Kaliya Hamlin, Planetwork)
* 12:00pm - 1:00pm Lunch buffet, in room
* Folding foundations into existing foundations.
* Starting up a new open source non-profit, Django.
* Foundation Umbrellas: Best practices for hosting groups of related and unrelated projects, fiscal sponsorship ([PDF](http://www.fiscalsponsorship.com/images/WCTEO_Gregory-Colvin.pdf)).

### Day 2: Round Table Discussion

* 9:00am - 9:30am Introductions
* Identity Umbrella Foundation
* Review action items: Topic for consideration - discussing the [action items from OSCON 2007](/node/4).
* Enterprise Adoption of Open Source: Donald would like to present some changes to the Eclipse Foundation Membership model to make The Eclipse Foundation more appealing and interesting to Enterprise *consumers* of Open Source. Donald would like to present the logic behind the changes, etc. It would be great to get a broader perspective of how Open Source participation has leaped from ISVs into a true ecosystem.
* Similar topic - any interest in having a community@ list at floss-foundations?
* [Autonomo.us](http://web.archive.org/web/20081002034721/http://autonomo.us/)
* [AVOIR](http://web.archive.org/web/20080701114454/http://avoir.uwc.ac.za/)
* University outreach, open course materials, educating students about free software
* ET phone home question, whether or what data to send back to a central repository
* 12:00pm - 1:00pm Lunch buffet, in room
* Trademarks - [List of policies](/node/21)
* Donors, Donations and Donor Relations: The OSI crew would like to chat about the broad topic of donors, donor relations and donations. What are best practices? What has worked for other groups? How to give donors visible social return on their donations, etc.
* Freeing of proprietary software, to push maintainership back to core project

Random [links](/node/16) mentioned in the conversations.

