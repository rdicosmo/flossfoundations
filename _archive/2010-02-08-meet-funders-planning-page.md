---
layout: single
title: "Meet the Funders Planning Page"
date: 2010-02-08
permalink: /meet-funders-planning-page
author: stormy
categories:
    - meetings
---

Purpose: Spread the word about free software among the large foundations that fund nonprofit work.

The plan:

Hold a "Meet the Funders" Event.

* Have two events, one in the Bay area (target companies and some Foundations) and one in the New York City area (target Foundations like the Ford Foundation.)
* Have one of the funders open the event. (Obviously one familiar with free software.)
* Give a very brief overview of free software and values and the organizations involved, who is involved, what they do to support free software and what funds are usually used.
* Have a poster session with refreshments and networking time.

Participants: East coast event, May, Learning Center, NYC:

* Stormy Peters
* Bradley M. Kuhn

West coast event, May 26th, Google offices in the Bay Area:

* Stormy Peters
* Jeff Sheltren

Decisions to be made:

* Dates:
    * Google will host the event in the Bay Area on May 26th.
    * Steve Holden is looking into a location for us in New York in the 5/2-5/14 date range.
    * Known conflicts from people who would like to help:
        * Jeffrey Altman: Spring HEPiX the week of April 19 to 23 and the AFS and Kerberos Best Practices Workshop is the week of May 24 to 29.
        * Stormy Peters: April 8-12, April 14-15: Linux Collaboration Summit (in San Francisco), April 18-25th: Peru free software event, April 29: Event at CU law school
        * Steve Holden: May 29 - June 19
        * Justin Erenkrantz: May 10-28 is my only window for availability right now.
* Locations. We have several offers to host in both locations.
    * New York:
        * Jeffrey Altman offered to get space at Baruch College in Manhattan.
        * Steve Holden offered that Holden Web, LLC will sponsor a venue in downtown Manhattan.
    * Bay Area:
        * Jeffrey Altman offered to get space at Stanford University.
        * Google Mountain View or San Francisco, April or May. (Proposed Tuesday April 20th or Wednesday 5/26.)
        * Mark Radcliffe offered their conference room in the Silicon Valley at University Avenue and Route 101.

Next steps:

* Choose dates
* Choose venue
* Define an agenda. Presentations from 2:30-4:30 about free software and organizations in general. Poster session, networking and cocktails from 4:30-6:30.
* Identify funders. If everyone could find 1-2 target people, that would be great! Perhaps people could look through their contacts and their LinkedIn lists and see who they might know ... or who might know someone that knows someone. We can share our leads via email.
* Invite funders.
* Get a sponsor for refreshments.
* Identify a funder to give the opening talk.
* Make the whole thing come off smoothly ...

Who's participating: (Organizations listed are the free software organizations that want to participate in the event. People should mark if they are interested in the east coast even, the west coast event or both.)

* Justin Erenkrantz, Apache (most likely only West Coast; Jim Jagielski, Greg Stein, and Serge Knystautas are all in the DC area, so one of those Apache folks may be able to attend an East Coast event)
* Steve Holden, Python Software Foundation
* Jeff Sheltren, OSU OSL - West Coast only
* Tyler Mitchell, Open Source Geospatial Foundation
* Jeffrey Altman, OpenAFS, East Coast
* Steve Holden, PSF
* Donald A. Lobo, CiviCRM, West Coast only
* Elin Waring, Open Source Matters, (Joomla!)
* Simon Phipps
* Alolita Sharma, OSI, West Coast only
* Mark Radcliffe
* Bradley M. Kuhn, Software Freedom Conservancy, East Coast Only
* Ian Skerrett, Eclipse Foundation
* Cornelius Schumacher, KDE
* Josh Berkus, Bay Area only
* Cat Allman
* David Mandel, LinuxFund
* Stormy Peters, GNOME Foundation, West Coast & East Coast
* Jim Fruchterman, Benetech, West Coast Only

---

## Comments

### Events

* Author: elin.waring
* Date: 2010-09-03 23:13

Yes for East as much as is possible without exact date.

Can I put a maybe for one of our board members for West?

Elin waring

Open Source Matters

Joomla! Project

### Could not edit the page :(

* Author: lobo
* Date: 2010-08-02 23:19

so adding my changes as a comment :)

1. I can check for space at the Mitch Kapor Foundation (in SF SOMA) once we settle on a date
2. Conflicts: DrupalCon is April 19-21, CiviCon is April 22

### Cannot Edit Page - Attendance

* Author: openafs
* Date: 2010-03-10 23:02

Jeffrey Altman will be attending the New York event on behalf of OpenAFS.

The currently scheduled date for the California event conflicts with the 2010 AFS and Kerberos Best Practices Workshop which I am organizing. I will try to arrange for an alternate to attend in my place. However, as most of the OpenAFS Elders will be at the Workshop, that may prove difficult.
